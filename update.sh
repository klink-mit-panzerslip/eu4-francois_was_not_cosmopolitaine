#!/bin/bash
ORIG=~/.steam/steam/steamapps/common/Europa\ Universalis\ IV/localisation
DEST=francois_was_not_so_cosmopolitaine/localisation
YAML="text_l_english.yml text_l_french.yml text_l_german.yml text_l_spanish.yml"

for file in $YAML; do
    # supposedly it can read twice the same string and keep the latest, so
    # one zzz-... file should be enough.
    cat "$ORIG/$file" | sed s/"cosmopolitan_french:0 \"Francien\""/"cosmopolitan_french:0 \"François\" # MOD francois was not"/g | sed s/"cosmopolitan_french:0 \"française\""/"cosmopolitan_french:0 \"françoise\" # MOD francois was not"/g | sed s/"cosmopolitan_french:0 \"Cosmopolitaine\""/"cosmopolitan_french:0 \"François\" # MOD francois was not"/g > "$DEST/$file"
    # just check if the string still exists
    echo "#### $ORIG/$file :"
    cat "$ORIG/$file" | grep osmopolit
done
