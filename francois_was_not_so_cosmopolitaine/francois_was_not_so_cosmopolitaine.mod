# use Francois instead of François in the name, since it looks like the steam workshop handle specific chars inconsistently
name="Francois was not so Cosmopolitaine after all"
path="mod/francois_was_not_so_cosmopolitaine"
supported_version = "1.18"
tags=
{
"Fixes"
 "Historical"
 "Cultures"
 "New Nations"
 }
picture="villers_cotterets.jpg"
